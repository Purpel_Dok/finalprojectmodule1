// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerPawn.generated.h"

class UCameraComponent;
class UFloatingPawnMovement;
class ABall;
class ABonus;

UCLASS()
class FINALPROJECTMD1_API APlayerPawn : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerPawn();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector LastDirection;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DeltaLocationX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 X;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Y;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DelaySeconds;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Points;

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* PawnCamera;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Component")
		UStaticMeshComponent* Cube;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Component")
		USceneComponent* SpawnPoint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Spawning")
		TSubclassOf<class ABall> BallToSpawn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector SpawnOfSet;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category= "Bonus");
	TSubclassOf<class ABonus> BonusClass;
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UFloatingPawnMovement* FloatingMovement;
	



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void MoveRight(float Axis);
	UFUNCTION()
		void PlayerCatr();
	UFUNCTION()
		void SpawnBall();
	UFUNCTION()
		void CallSpawnBall();
	UFUNCTION()
		void SpawnBonus();




};
