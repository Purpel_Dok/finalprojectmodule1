// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h" 
#include "Bonus.generated.h"

UCLASS()
class FINALPROJECTMD1_API ABonus : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABonus();
	UPROPERTY(EditAnywhere, Category= "Bonus")
		float Speed = 100.0f;

	UPROPERTY(EditAnywhere, Category= "Bonus")
		UStaticMeshComponent* BonusMesh;

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category= "Bonus")
		float BonusDuration;

	UPROPERTY(EditAnywhere, Category= "Bonus")
		bool bIsCollected = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent, Category= "Bonus")
		void OnPickup(AActor* Collector);

	UFUNCTION(BlueprintImplementableEvent, Category= "Bonus")
		void ActivateBonus();
};
