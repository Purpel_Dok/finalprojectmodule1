// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FinalProjectMd1GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FINALPROJECTMD1_API AFinalProjectMd1GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
