// Fill out your copyright notice in the Description page of Project Settings.


#include "BlockGeneration.h"
#include "Block.h"
#include "PlayerPawn.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABlockGeneration::ABlockGeneration()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	I = 4;
	J = 7;
	BlockSize = 350;
	Distance = 50.f;

}

// Called when the game starts or when spawned
void ABlockGeneration::BeginPlay()
{
	Super::BeginPlay();

	

	for(int32 a = 0; a < I; a++)
	{
		for(int32 b = 0; b < J; b++)
		{
			FVector SpawnDistance = GetActorLocation() + FVector(Distance * a, 0.f, 0.f);
			FVector BlockLocation = Generation(a, b);
			SpawnBlock();
			ABlock* NewBlock = GetWorld()->SpawnActor<ABlock>(BlockToSpawn, BlockLocation, FRotator::ZeroRotator);
		}
	}
	APlayerPawn* Player = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	Player->X = J;
	Player->Y = J;
}

// Called every frame
void ABlockGeneration::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlockGeneration::SpawnBlock()
{
	
	const FVector SpawnLocation = GetActorLocation();
	const FRotator SpawnRotation = GetActorRotation();
	ABlock* SpawnedBlock = GetWorld()->SpawnActor<ABlock>(BlockToSpawn, SpawnLocation, SpawnRotation);
}

FVector ABlockGeneration::Generation(int a, int b)
{
	
	return GetActorLocation() + FVector(a * BlockSize, b * BlockSize, 0);
}

