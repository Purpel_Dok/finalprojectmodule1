// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Bonus.h"
#include "BonusPaltformWidth.generated.h"

/**
 * 
 */
UCLASS()
class FINALPROJECTMD1_API ABonusPaltformWidth : public ABonus
{
	GENERATED_BODY()

	void ActivateBonus();
	
};
