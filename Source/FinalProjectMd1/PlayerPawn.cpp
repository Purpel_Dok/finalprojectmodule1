// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Ball.h"
#include "Bonus.h"


// Sets default values
APlayerPawn::APlayerPawn()
{
    // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
    RootComponent = PawnCamera;
    Cube = CreateDefaultSubobject<UStaticMeshComponent>(TEXT ("Cube"));
    Cube->SetRelativeScale3D(FVector(0.5, 6, 2));
    SpawnPoint = CreateDefaultSubobject<USceneComponent>(TEXT("SpawnPoint"));
    SpawnPoint->SetRelativeLocation(FVector(200, 0, 0));


    Speed = 1600.f;
    LastDirection = FVector(0, 0, 0);
    DeltaLocationX = 0;
    X = 0;
    Y = 0;

}


// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
    Super::BeginPlay();
    CallSpawnBall();

}



// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis("MoveRight", this, &APlayerPawn::MoveRight);
   
}

void APlayerPawn::MoveRight(float Axis)
{
    const float MinLimit = -2378.f;
    const float MaxLimit = 2378.f;
    auto Igric = LastDirection.Y;
    LastDirection.Y = FMath::Clamp(Igric, MinLimit, MaxLimit);
    LastDirection = FVector(0, Axis * Speed * GetWorld()->GetDeltaSeconds(), 0);
    AddActorWorldOffset(LastDirection, true);
    
}

void APlayerPawn::PlayerCatr()
{
	static ConstructorHelpers::FClassFinder<ABall> MyBallObj(TEXT("Blueprint'/Game/Blueprint/BP_Ball.BP_Ball'"));

		if (MyBallObj.Succeeded())
			{
			    BallToSpawn = MyBallObj.Class;
			}
}

void APlayerPawn::SpawnBall()
{
    const FVector SpawnLocation = SpawnPoint->GetComponentLocation();
    const FRotator SpawnRotation = GetActorRotation();

    ABall* NewBall = GetWorld()->SpawnActor<ABall>(BallToSpawn, SpawnLocation, SpawnRotation);
}

void APlayerPawn::CallSpawnBall()
{
    FTimerHandle TimerHandle;
    GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &APlayerPawn::SpawnBall, DelaySeconds, false);
}

void APlayerPawn::SpawnBonus()
{
    FVector SpawnLocation = GetActorLocation();
    FRotator SpawnRotation = GetActorRotation();

    ABonus* Bonus = GetWorld()->SpawnActor<ABonus>(BonusClass, SpawnLocation,SpawnRotation);
    Bonus->SetActorScale3D(FVector(0.5f,0.5f,0.5f));

    //ActiveBonuses.Add(Bonus);
}





