// Fill out your copyright notice in the Description page of Project Settings.


#include "Ball.h"
#include "Particles/ParticleSystemComponent.h"
#include "PlayerPawn.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/KillZVolume.h"


// Sets default values
ABall::ABall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Speed = 888;

	B_Ball = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Sphere"));
	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	Arrow->SetupAttachment(B_Ball);
	Arrow->SetRelativeLocation(FVector(50, 0, 0));

}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	Direction = GetActorRotation().Vector();
}


// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FHitResult Fhr;
	AddActorWorldOffset(Direction * Speed * DeltaTime, true, &Fhr);

	if (Fhr.bBlockingHit)
	{
		Direction = FMath::GetReflectionVector(Direction, Fhr.Normal);
		SetActorRotation(Direction.ToOrientationRotator());
	}
}

void ABall::NotifyHit(UPrimitiveComponent* MyComp,
					AActor* Other, 
					UPrimitiveComponent* OtherComp, 
					bool bSelfMoved, FVector HitLocation,
					FVector HitNormal, FVector NormalImpulse,
					const FHitResult& Hit)
{
	APlayerPawn* Player = Cast<APlayerPawn>(Other);
	Speed = Speed + 10;
	if (Player)
	{
		Direction = Direction + Player->LastDirection.GetSafeNormal();
		Direction = Direction.GetSafeNormal();
	}
}

void ABall::NotifyActorBeginOverlap(AActor* OtherActor)
{
	AKillZVolume* DeathTime = Cast<AKillZVolume>(OtherActor);
	if (DeathTime)
	{
		PlayerDeath();
		K2_DestroyActor();
	}
}

