// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/Actor.h"
#include "Ball.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

UCLASS()
class FINALPROJECTMD1_API ABall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABall();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Speed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector Direction;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components");
		UStaticMeshComponent* B_Ball;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components");
		UArrowComponent* Arrow;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components");
		UAudioComponent* Audio;
		UPROPERTY(BlueprintAssignable, BlueprintReadWrite, Category = "EventDispatchers")
			FOnDead PlayerDead;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void NotifyActorBeginOverlap(AActor* OtherActor) override;
	UFUNCTION()
		void NotifyHit(UPrimitiveComponent* MyComp,
						AActor* Other, 
						UPrimitiveComponent* OtherComp, 
						bool bSelfMoved, FVector HitLocation, 
						FVector HitNormal, FVector NormalImpulse,
						const FHitResult& Hit) override;

	UFUNCTION(BlueprintImplementableEvent)
		void PlayerDeath();

	
};
