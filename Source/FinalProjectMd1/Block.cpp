// Fill out your copyright notice in the Description page of Project Settings.


#include "Block.h"

#include "Ball.h"
#include "BonusPaltformWidth.h"
#include "BonusSpeed.h"
#include "PlayerPawn.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABlock::ABlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Health = 2;
	Cube = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CUBE"));
}

// Called when the game starts or when spawned
void ABlock::BeginPlay()
{
	Super::BeginPlay();
	
}



// Called every frame
void ABlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlock::OnBlockDestroeyed()
{
	TArray<TSubclassOf<ABonus>> BonusClasses =
		{ABonusPaltformWidth::StaticClass(), ABonusSpeed::StaticClass()};
	TSubclassOf<ABonus> BonusClass = BonusClasses[FMath::RandRange(0, BonusClasses.Num() - 1)];

	ABonus* Bonus = GetWorld()->SpawnActor<ABonus>(BonusClass, GetActorLocation(), FRotator::ZeroRotator);
	
}

void ABlock::NotifyHit(UPrimitiveComponent* MyComp, 
                       AActor* Other, 
                       UPrimitiveComponent* OtherComp,
                       bool bSelfMoved,
                       FVector HitLocation, 
                       FVector HitNormal, 
                       FVector NormalImpulse, 
                       const FHitResult& Hit)
{
	ABall* obj = Cast<ABall>(Other);
	if(obj)
	{
		Health -= 1;
		if(Health <= 0)
		{
			K2_DestroyActor();
			APlayerPawn* Player = Cast<APlayerPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
			Player->Points += 1;
		}
		else
		{
			SetMaterial();
		}
	}
}
