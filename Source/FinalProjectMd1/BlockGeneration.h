// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Block.h"
#include "GameFramework/Actor.h"
#include "BlockGeneration.generated.h"

UCLASS()
class FINALPROJECTMD1_API ABlockGeneration : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlockGeneration();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 I;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 J;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 BlockSize;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Distance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawning")
		TSubclassOf<ABlock> BlockToSpawn;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void SpawnBlock();
	UFUNCTION()
		FVector Generation(int a, int b);
};
