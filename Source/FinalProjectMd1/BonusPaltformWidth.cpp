// Fill out your copyright notice in the Description page of Project Settings.


#include "BonusPaltformWidth.h"

#include "PlayerPawn.h"

void ABonusPaltformWidth::ActivateBonus()
{
	APlayerPawn* PlayerPawn = Cast<APlayerPawn>(GetWorld()->GetFirstPlayerController()->GetPawn());
	PlayerPawn->SetActorScale3D(FVector(PlayerPawn->GetActorScale().X, PlayerPawn->GetActorScale(). Y + 50.0f, PlayerPawn->GetActorScale(). Z));
}
