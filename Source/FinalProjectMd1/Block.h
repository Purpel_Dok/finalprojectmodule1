// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Block.generated.h"

UCLASS()
class FINALPROJECTMD1_API ABlock : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlock();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 Health;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* Cube;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void NotifyHit(UPrimitiveComponent* MyComp, 
				AActor* Other, 
				UPrimitiveComponent* OtherComp, 
				bool bSelfMoved, 
				FVector HitLocation, 
				FVector HitNormal,
				FVector NormalImpulse, 
				const FHitResult& Hit) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent)
		void SetMaterial();
	UFUNCTION()
		void OnBlockDestroeyed();

};
